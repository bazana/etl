package com.bazana;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.document.*;
import com.amazonaws.services.dynamodbv2.model.*;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.batch.item.support.ListItemWriter;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class ViewItemWriter extends ListItemWriter<ViewItem> {
    private final DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("dd/MM/yyyy'T'HH:mm:ss'Z");
    DynamoDB dynamoDB;
    private final String TABLE_NAME = "views";

    @Override
    public void write(List<? extends ViewItem> items) throws Exception {
        AmazonDynamoDBClient client = getDynamoDBClient();

        dynamoDB = new DynamoDB(client);

// Test for local DynamoDB instance
//        createExampleTable();
//        listMyTables();
//        getTableInformation();

        TableWriteItems twi = new TableWriteItems(TABLE_NAME);

        for(ViewItem viewItem : items) {
                    twi.addItemToPut(new Item()
                            .withPrimaryKey("id", viewItem.getId())
                            .withString("CONTENT_SR_KEY",  viewItem.getContentSrKey())
                            .withString("SESSION_ENDDATE_TIME",  viewItem.getSessionEnddateTime().toString(dateTimeFormatter))
                            .withString("SESSION_STARTDATE_TIME",  viewItem.getSessionStartdateTime().toString(dateTimeFormatter))
                            .withString("SUBSCRIPTION_SR_KEY",  viewItem.getSubscriptionSrKey())
                            .withString("VIEWING_COUNTRY_SR_KEY",  viewItem.getViewingCountrySrKey()));

        }

        try {

            System.out.println("Making the request.");
            BatchWriteItemOutcome outcome = dynamoDB.batchWriteItem(twi);

            do {

                // Check for unprocessed keys which could happen if you exceed provisioned throughput
                Map<String, List<WriteRequest>> unprocessedItems = outcome.getUnprocessedItems();

                if (outcome.getUnprocessedItems().size() == 0) {
                    System.out.println("No unprocessed items found");
                } else {
                    System.out.println("Retrieving the unprocessed items");
                    outcome = dynamoDB.batchWriteItemUnprocessed(unprocessedItems);
                }

            } while (outcome.getUnprocessedItems().size() > 0);

            System.out.println("Batch processed.");

        } catch (Exception e) {
            System.err.println("Unable to add viewItem.");
            e.printStackTrace();
        }
    }

    private void createExampleTable() {

        try {

            ArrayList<AttributeDefinition> attributeDefinitions = new ArrayList<AttributeDefinition>();
            attributeDefinitions.add(new AttributeDefinition()
                    .withAttributeName("Id")
                    .withAttributeType("N"));

            ArrayList<KeySchemaElement> keySchema = new ArrayList<KeySchemaElement>();
            keySchema.add(new KeySchemaElement()
                    .withAttributeName("Id")
                    .withKeyType(KeyType.HASH)); //Partition key

            CreateTableRequest request = new CreateTableRequest()
                    .withTableName(TABLE_NAME)
                    .withKeySchema(keySchema)
                    .withAttributeDefinitions(attributeDefinitions)
                    .withProvisionedThroughput(new ProvisionedThroughput()
                            .withReadCapacityUnits(5L)
                            .withWriteCapacityUnits(6L));

            System.out.println("Issuing CreateTable request for " + TABLE_NAME);
            Table table = dynamoDB.createTable(request);

            System.out.println("Waiting for " + TABLE_NAME
                    + " to be created...this may take a while...");
            table.waitForActive();

            getTableInformation();

        } catch (Exception e) {
            System.err.println("CreateTable request failed for " + TABLE_NAME);
            System.err.println(e.getMessage());
        }

    }

    private void listMyTables() {

        TableCollection<ListTablesResult> tables = dynamoDB.listTables();
        Iterator<Table> iterator = tables.iterator();

        System.out.println("Listing table names");

        while (iterator.hasNext()) {
            Table table = iterator.next();
            System.out.println(table.getTableName());
        }
    }

    private void getTableInformation() {

        System.out.println("Describing table." );

        TableDescription tableDescription = dynamoDB.getTable(TABLE_NAME).describe();
        System.out.format("Name: %s:\n" + "Status: %s \n"
                        + "Provisioned Throughput (read capacity units/sec): %d \n"
                        + "Provisioned Throughput (write capacity units/sec): %d \n",
                tableDescription.getTableName(),
                tableDescription.getTableStatus(),
                tableDescription.getProvisionedThroughput().getReadCapacityUnits(),
                tableDescription.getProvisionedThroughput().getWriteCapacityUnits());
    }


    private AmazonDynamoDBClient getDynamoDBLocalClient(){
        AmazonDynamoDBClient dbLocalClient = new AmazonDynamoDBClient();
        dbLocalClient.setEndpoint("http://localhost:8000");
        return dbLocalClient;
    }

    private AmazonDynamoDBClient getDynamoDBClient(){
        AmazonDynamoDBClient dbLocalClient = new AmazonDynamoDBClient();
        dbLocalClient.setRegion(Region.getRegion(Regions.EU_WEST_1));
        return dbLocalClient;
    }

}
