package com.bazana;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.UrlResource;

import java.net.MalformedURLException;

@Configuration
@EnableBatchProcessing
@EnableAutoConfiguration
public class IDGeneratorBatchConfiguration {

    @Autowired
    public JobBuilderFactory jobBuilderFactory;

    @Autowired
    public StepBuilderFactory stepBuilderFactory;

    // tag::readerwriterprocessor[]
    @Bean
    public FlatFileItemReader<ViewItem> reader() {
        FlatFileItemReader<ViewItem> reader = new FlatFileItemReader<ViewItem>();
        try {
            reader.setResource(new UrlResource("file:///tmp/data.csv"));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        reader.setLineMapper(new DefaultLineMapper<ViewItem>() {{
            setLineTokenizer(new DelimitedLineTokenizer() {{
                setNames(new String[] { "subscriptionSrKey", "contentSrKey", "sessionStartdateTime", "sessionEnddateTime", "viewingCountrySrKey" });
            }});
            setFieldSetMapper(new ItemFieldSetMapper());
        }});
        return reader;
    }

    @Bean
    public ItemProcessor processor() {
        return new ViewItemProcessor();
    }

    @Bean
    public ViewItemWriter writer() {
        return new ViewItemWriter();
    }
    // end::readerwriterprocessor[]

    // tag::listener[]

    @Bean
    public JobExecutionListener listener() {
        return new JobCompletionNotificationListener();
    }

    // end::listener[]

    // tag::jobstep[]
    @Bean
    public Job importUserJob() {
        return jobBuilderFactory.get("importJob")
                .incrementer(new RunIdIncrementer())
                .listener(listener())
                .flow(step1())
                .end()
                .build();
    }

    @Bean
    public Step step1() {
        return stepBuilderFactory.get("step1")
                .<ViewItem, ViewItem> chunk(25)
                .reader(reader())
                .processor(processor())
                .writer(writer())
                .build();
    }
    // end::jobstep[]
}