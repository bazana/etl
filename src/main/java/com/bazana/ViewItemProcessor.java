package com.bazana;

import org.springframework.batch.item.ItemProcessor;

import java.util.UUID;

public class ViewItemProcessor implements ItemProcessor<ViewItem, ViewItem> {
    @Override
    public ViewItem process(ViewItem viewItem) throws Exception {
        ViewItem transFormedViewItem = viewItem;
        transFormedViewItem.setUuid(UUID.randomUUID());
        return transFormedViewItem;
    }
}
