package com.bazana;

import org.springframework.boot.SpringApplication;

public class Application {
    public static void main(String [] args) {

        if(args.length == 0)
            System.exit(SpringApplication.exit(SpringApplication.run(
                    BatchConfiguration.class, args)));
        else
            System.exit(SpringApplication.exit(SpringApplication.run(
                    IDGeneratorBatchConfiguration.class, args)));
    }
}