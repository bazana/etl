package com.bazana;

import org.joda.time.DateTime;

import java.util.UUID;

/**
 * Created by jessie on 29/07/2016.
 */
public class ViewItem {

    private String id;
    private UUID uuid;
    private String subscriptionSrKey;
    private String contentSrKey;
    private DateTime sessionStartdateTime;
    private DateTime sessionEnddateTime;
    private String viewingCountrySrKey;

    public ViewItem(){}

    public ViewItem(String id, String subscriptionSrKey, String contentSrKey, DateTime sessionStartdateTime, DateTime sessionEnddateTime, String viewingCountrySrKey) {
        this.id = id;
        this.subscriptionSrKey = subscriptionSrKey;
        this.contentSrKey = contentSrKey;
        this.sessionStartdateTime = sessionStartdateTime;
        this.sessionEnddateTime = sessionEnddateTime;
        this.viewingCountrySrKey = viewingCountrySrKey;
    }

    public String getSubscriptionSrKey() {
        return subscriptionSrKey;
    }

    public String getContentSrKey() {
        return contentSrKey;
    }

    public void setContentSrKey(String contentSrKey) {
        this.contentSrKey = contentSrKey;
    }

    public DateTime getSessionStartdateTime() {
        return sessionStartdateTime;
    }

    public void setSessionStartdateTime(DateTime sessionStartdateTime) {
        this.sessionStartdateTime = sessionStartdateTime;
    }

    public DateTime getSessionEnddateTime() {
        return sessionEnddateTime;
    }

    public void setSessionEnddateTime(DateTime sessionEnddateTime) {
        this.sessionEnddateTime = sessionEnddateTime;
    }

    public String getViewingCountrySrKey() {
        return viewingCountrySrKey;
    }

    public void setViewingCountrySrKey(String viewingCountrySrKey) {
        this.viewingCountrySrKey = viewingCountrySrKey;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public void setSubscriptionSrKey(String subscriptionSrKey) {
        this.subscriptionSrKey = subscriptionSrKey;
    }

    public String getId() {
        if (id == null)
            return getUuid().toString();
        else
            return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "ViewItem{" +
                "id='" + id + '\'' +
                ", uuid=" + uuid +
                ", subscriptionSrKey='" + subscriptionSrKey + '\'' +
                ", contentSrKey='" + contentSrKey + '\'' +
                ", sessionStartdateTime=" + sessionStartdateTime +
                ", sessionEnddateTime=" + sessionEnddateTime +
                ", viewingCountrySrKey='" + viewingCountrySrKey + '\'' +
                '}';
    }
}
