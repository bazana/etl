package com.bazana;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

/**
 * Created by jessie on 29/07/2016.
 */
public class ItemFieldSetMapper implements FieldSetMapper<ViewItem> {
    DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss.000");

    @Override
    public ViewItem mapFieldSet(FieldSet fieldSet) throws BindException {
        ViewItem viewItem = new ViewItem();
        viewItem.setContentSrKey(fieldSet.readString("contentSrKey"));
        viewItem.setViewingCountrySrKey(fieldSet.readString("viewingCountrySrKey"));
        viewItem.setSubscriptionSrKey(fieldSet.readString("subscriptionSrKey"));
        DateTime sessionStartdateTime = fmt.parseDateTime(fieldSet.readString("sessionStartdateTime"));
        viewItem.setSessionStartdateTime(sessionStartdateTime);
        DateTime sessionEnddateTime = fmt.parseDateTime(fieldSet.readString("sessionEnddateTime"));;
        viewItem.setSessionEnddateTime(sessionEnddateTime);

        if(fieldSet.getFieldCount() == 6)
            viewItem.setId(fieldSet.readRawString("id"));

        return viewItem;
    }
}
