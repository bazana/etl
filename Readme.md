# Building
./gradlew clean build

# Copy data file
- Copy CSV data file in location /tmp/data.csv

# ETL to generate IDs

CSV must be in format but with no header.

	SUBSCRIPTION_SR_KEY,CONTENT_SR_KEY,SESSION_STARTDATE_TIME,SESSION_ENDDATE_TIME,VIEWING_COUNTRY_SR_KEY
	490777,20956,2016-07-28 09:48:59.000,2016-07-28 09:53:06.000,14

Run jar

java -jar build/libs/dynamodb-csv-importer-1.0-SNAPSHOT.jar ids

# ETL with generated IDs
CSV must be in format but with no header.

	id,SUBSCRIPTION_SR_KEY,CONTENT_SR_KEY,SESSION_STARTDATE_TIME,SESSION_ENDDATE_TIME,VIEWING_COUNTRY_SR_KEY
	1234,490777,20956,2016-07-28 09:48:59.000,2016-07-28 09:53:06.000,14

java -jar build/libs/dynamodb-csv-importer-1.0-SNAPSHOT.jar

